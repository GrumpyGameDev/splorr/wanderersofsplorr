using System;

namespace wanderersofsplorr.Models
{
    public class AccountGenerateResponseModel
    {
        public string Token { get; set; }

    }
}
