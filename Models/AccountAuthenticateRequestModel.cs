using System;

namespace wanderersofsplorr.Models
{
    public class AccountAuthenticateRequestModel
    {
        public string Token { get; set; }

    }
}
