using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using wanderersofsplorr.Models;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace wanderersofsplorr.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IConfiguration _configuration;
        public AccountController(
            ILogger<AccountController> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public ActionResult Generate()
        {
            string usernameString = Guid.NewGuid().ToString();
            var handler = new JwtSecurityTokenHandler();
            var descriptor = new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor
            {
                Audience = _configuration["JwtAuth:Audience"],
                Issuer = _configuration["JwtAuth:Issuer"],
                Expires = System.DateTime.UtcNow.AddDays(1),
                NotBefore = System.DateTime.UtcNow,
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, usernameString)
                }),
                SigningCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(
                    new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration["JwtAuth:Secret"])),
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256")
            };
            var token = handler.CreateToken(descriptor);
            return View(new Models.AccountGenerateResponseModel{ Token = handler.WriteToken(token)});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Authenticate(Models.AccountAuthenticateRequestModel model)
        {
            var handler = new JwtSecurityTokenHandler();
            var principal=handler.ValidateToken(model.Token,
                new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuer=true,
                    ValidateAudience=true,
                    ValidateLifetime=true,
                    IssuerSigningKey=new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration["JwtAuth:Secret"])),
                    ValidIssuer=_configuration["JwtAuth:Issuer"],
                    ValidAudience=_configuration["JwtAuth:Audience"]
                },out var validated);
            
            return View();
        }

    }
}
